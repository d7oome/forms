namespace FUIAS4.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using FUIAS4.Models;

    public partial class Database : DbContext
    {
        public Database()
            : base("name=Database")
        {
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Category> Categorys { get; set; }
        public virtual DbSet<FormCat> FormCats { get; set; }
        public virtual DbSet<Forms> Forms { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserForm> UserForms { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
    }
}
