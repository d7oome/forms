﻿namespace FUIAS4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FormCats",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Category_Id = c.Int(),
                        forms_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.Category_Id)
                .ForeignKey("dbo.Forms", t => t.forms_Id)
                .Index(t => t.Category_Id)
                .Index(t => t.forms_Id);
            
            CreateTable(
                "dbo.Forms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Path = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserForms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateTime = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        forms_Id = c.Int(),
                        user_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Forms", t => t.forms_Id)
                .ForeignKey("dbo.Users", t => t.user_Id)
                .Index(t => t.forms_Id)
                .Index(t => t.user_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        F_Name = c.String(),
                        M_Name = c.String(),
                        L_Name = c.String(),
                        Age = c.Int(nullable: false),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                        EmplooyeNumber = c.Int(nullable: false),
                        PassowrdSalt = c.Binary(),
                        PasswordHash = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        role_Id = c.Int(),
                        user_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.role_Id)
                .ForeignKey("dbo.Users", t => t.user_Id)
                .Index(t => t.role_Id)
                .Index(t => t.user_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRoles", "user_Id", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "role_Id", "dbo.Roles");
            DropForeignKey("dbo.UserForms", "user_Id", "dbo.Users");
            DropForeignKey("dbo.UserForms", "forms_Id", "dbo.Forms");
            DropForeignKey("dbo.FormCats", "forms_Id", "dbo.Forms");
            DropForeignKey("dbo.FormCats", "Category_Id", "dbo.Categories");
            DropIndex("dbo.UserRoles", new[] { "user_Id" });
            DropIndex("dbo.UserRoles", new[] { "role_Id" });
            DropIndex("dbo.UserForms", new[] { "user_Id" });
            DropIndex("dbo.UserForms", new[] { "forms_Id" });
            DropIndex("dbo.FormCats", new[] { "forms_Id" });
            DropIndex("dbo.FormCats", new[] { "Category_Id" });
            DropTable("dbo.UserRoles");
            DropTable("dbo.Users");
            DropTable("dbo.UserForms");
            DropTable("dbo.Roles");
            DropTable("dbo.Forms");
            DropTable("dbo.FormCats");
            DropTable("dbo.Categories");
        }
    }
}
