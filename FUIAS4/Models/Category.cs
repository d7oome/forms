﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FUIAS4.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int IsDeleted { get; set; }

    }
}