﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FUIAS4.Models
{
    public class FormCat
    {
        public int Id { get; set; }
        public Forms forms { get; set; }
        public Category Category { get; set; }

    }
}