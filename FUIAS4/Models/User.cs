﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FUIAS4.Models
{
    public class User
    {
        public int Id { get; set; }
        public string F_Name { get; set; }
        public string M_Name { get; set; }
        public string L_Name { get; set; }
        public int Age { get; set; }
        public string PhoneNumber { get; set; }

        [EmailAddress]
        public string Email { get; set; }
        public int EmplooyeNumber { get; set; }
        public byte[] PassowrdSalt { get; set; }
        public byte[] PasswordHash { get; set; }
        public int IsDeleted { get; set; }
    }
}