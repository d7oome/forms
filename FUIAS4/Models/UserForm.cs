﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace FUIAS4.Models
{
    public class UserForm
    {
        public int Id { get; set; }
       
        public DateTime DateTime { get; set; }
        public int Status { get; set; }
        public User user { get; set; }
        public Forms forms { get; set; }

    }
}