﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FUIAS4.Models
{
    public class UserRole
    {

        public int Id { get; set; }
        public  User user { get; set; }
        public Role role { get; set; }
    }
}